PUBLIC XCER
PUBLIC YCER

DATA2 SEGMENT PARA PUBLIC 'DATA'
	
	; tabele cu valori precalculate ale sinusului si cosinusului pentru unghiuri intre 0 si 360 din 10 in 10 grade
	
	SIN DD 0.0,0.173643,0.34201,0.499987,0.642772,0.766028,0.86601,0.93968,0.984801,1.0,0.984817,0.939712,0.866056,0.766087,0.642843,0.500067,0.342098,0.173734,0.0000926536,-0.173552,-0.341923,-0.499906,-0.642701,-0.765968,-0.865964,-0.939649,-0.984785,-1.0,-0.984833,-0.939744,-0.866103,-0.766147,-0.642914,-0.500147,-0.342185,-0.173826
	COS DD 1.0,0.984809,0.939696,0.866033,0.766058,0.642807,0.500027,0.342054,0.173689,0.0000463268,-0.173597,-0.341967,-0.499947,-0.642736,-0.765998,-0.865987,-0.939664,-0.984793,-1.0,-0.984825,-0.939728,-0.866079,-0.766117,-0.642878,-0.500107,-0.342141,-0.17378,-0.00013898,0.173506,0.34188,0.499866,0.642665,0.765939,0.86594,0.939633,0.984776
	
	XCER DW 40 dup(?)
	YCER DW 40 dup(?)
	
DATA2 ENDS

sablon STRUC
	BP4 DW ?
	AE5 DW ?
	AS5 DW ?
	cercxc DW ?
	cercyc DW ?
	cercr DW ?
sablon ENDS

INCLUDE C:\Tasm\a_math.mac

PUBLIC compute_coord

CODE2 SEGMENT PARA PUBLIC 'CODE'
	ASSUME CS:CODE2, DS:DATA2
	
	compute_coord PROC FAR
		PUSH BP
		MOV BP, SP
		
		PUSH AX
		PUSH CX
		PUSH SI
		PUSH BX
		
		; xcer = xc + raza * cos
		; ycer = yc + raza * sin
		
		MOV CX, 36 ; se calculeaza coordonatele punctelor de pe cercul pe care dorim sa il afisam
		MOV SI, 0
		MOV BX, 0
		
		MOV AX, DATA2
		MOV DS, AX
		
		MOV AX, 1
		
	LOOP1:
		
		FINIT ; folosim coprocesorul matematic pentru a calcula coordonatele
		
		FILD [BP].cercxc
		FILD [BP].cercr
		FLD DS:COS[SI]
		FMUL
		FADD
		FIST DS:XCER[BX] ; coordonatele calculate se salveaza intr-un tablou cu valori de tip word
		
		FWAIT
		
		FINIT
		
		FILD [BP].cercyc
		FILD [BP].cercr
		FLD DS:SIN[SI]
		FMUL 
		FADD
		FIST DS:YCER[BX]
		
		FWAIT
		
		ADD SI, AX	; se trece la urmatoarea valoare a sinusului
		ADD SI, AX
		ADD SI, AX
		ADD SI, AX
		
		ADD BX, 2 ; se trece la urmatoarea valoare din tabelul cu coordonate calculate
		
		SUB CX, AX
		CMP CX, 0
		JG LOOP1
		
		POP BX
		POP SI
		POP CX
		POP AX
		POP BP ; restaurare registre
		
		RET 6 ; eliberare memorie pentru parametrii transmisi prin stiva
	compute_coord ENDP


CODE2 ENDS
END