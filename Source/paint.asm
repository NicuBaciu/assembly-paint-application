DATA SEGMENT PARA PUBLIC 'DATA'
	ct_color DW ?
	ct_fill DW ?
	
	pixel_x DW ?
	pixel_y DW ?
	
	line_x1 DW ?
	line_y1 DW ?
	
	line_x2 DW ?
	line_y2 DW ?
	
	poli_n DW ?
	poli DW 72 dup(?)
	
	cerc_xc DW ?
	cerc_yc DW ?
	cerc_r DW ?
	
	deltax DW 0
	deltay DW 0
	
	poligon1 DW 220, 75, 420, 75, 420, 275, 220, 275, 220, 75
	poligon2 DW 320, 150, 300, 195, 340, 195, 320, 150
	poligon3 DW 220, 75, 286, 50, 352, 50, 420, 75, 445, 141, 445, 207, 420, 275, 352, 300, 286, 300, 220, 275, 195, 209, 195, 143, 220, 75
	
	badinputstring DB 'You gave a bad input! Please try giving an input again but be careful, it has   to meet the conditions stated. Press any key to try again.',10,13,'$'
	
	headerstring DB '================================== PAINT ASM ===================================','                                                                   Baciu Nicolae',10,13, '$'
	
	stringmeniu DB 'This is a program that lets the user choose a shape, write its coordinates and  color and display the chosen shape on the screen. It provides a canvas of       640 x 350 pixels.',10,13,'BE CAREFULL! When printing polygons, avoid concave polygons. The program was notdesigned to handle such polygons.',10,13,'To exit the program press ESC!',10,13,'To choose what shape you want to draw write the coresponding number:',10,13,10,13,'1. Draw pixel',10,13,'2. Draw line',10,13,'3. Draw polygon',10,13,'4. Draw circle',10,13,'$'
	
	pixelstring0 DB 'To print a pixel you have to provide the x and y coordinates as well as the     color.',10,13,10,13,'x: $'
	linestring0 DB 'To print a line you have to provide the x and y coordinates of both ends as wellas the color.',10,13,10,13,'x1: $'
	polistring0 DB 'To print a poligon you have to provide the number of vertices and then the x andy coordinates of every one as well as the color.',10,13,'The number of vertices of the polygon should not exceed 36!',10,13,10,13,'n: $'
	cercstring0 DB 'To print a circle you have to provide the x and y coordinates of the center, theradius as well as the color.',10,13,10,13,'x center: $'
	
	pixelstring1 DB 10,13,'y: $'
	
	linestring1 DB 10,13,'y1: $'
	linestring2 DB 10,13,'x2: $'
	linestring3 DB 10,13,'y2: $'
		
	polistringx DB 10,13,'x: $'
	polistringy DB 10,13,'y: $'
	polistringCT_VERTEX DB 10,13,'Please provide the coordinates of the vertex number $'
	
	cercstring1 DB 10,13,'y center: $'
	cercstring2 DB 10,13,'radius: $'
	
	colorstring DB 10,13,'Please provide a color to draw the shape. The available colors are as follows:',10,13,'0 - Black',10,13,'1 - Blue',10,13,'2 - Green',10,13,'3 - Cyan',10,13,'4 - Red',10,13,'5 - Magenta',10,13,'6 - Brown',10,13,'7 - Light Gray',10,13,'8 - Dark Grey',10,13,'9 - Light Blue',10,13,'A - Light Green',10,13,'B - Light Cyan',10,13,'C - Light Red',10,13,'D - Light Magenta',10,13,'E - Yellow',10,13,'F - White',10,13,'$'
	fillstring DB 10,13,'The shape you have chosen can be display as a wireframe or as a filled shape. Please type:',10,13,'0 for wireframe',10,13,'1 for fill',10,13,10,13,'$'
	
	badinputstring1 DB 10,13,'Please provide an integer between 0 and 640!',10,13,'$'
	badinputstring2 DB 10,13,'Please provide an integer between 0 and 350!',10,13,'$'
	badinputstringCOLOR DB 10,13,'Please provide a valid color from the colors below!',10,13,'$'
	badinputstringNR DB 10,13,'Please provide an integer between 2 and 36!',10,13,'$'
	badinputstringFILL DB 10,13,'Please input 1 for fill or 0 for wireframe!',10,13,'$'
	badinputstringRADIUS DB 10,13,'Radius too big! Please input a radius smaller then $'
	
tab_proc DW in_pixel
		DW in_line
		DW in_poli
		DW in_cerc
DATA ENDS

INCLUDE C:\Tasm\a_pixel.mac

EXTRN print_pixel:FAR
EXTRN print_line:FAR
EXTRN print_tr:FAR
EXTRN print_poligon:FAR
EXTRN print_circle:FAR
EXTRN compute_sin:FAR

CODE SEGMENT PARA PUBLIC 'CODE'
	START:
		ASSUME CS:CODE, DS:DATA, ES:DATA
		PUSH DS
		XOR AX,AX
		PUSH AX
		MOV AX,DATA; initialise DS
		MOV DS, AX
		
		CALL clear_screen
		
	MAINMENU:
		MOV AH, 09h
		LEA DX, headerstring
		INT 21h
	
		MOV AH, 09h
		LEA DX, stringmeniu
		INT 21h
		
		CALL read_ch
		CMP AL, '4'		; verifica daca e intre 1 si 4
		JG BADINPUT1
		CMP AL, '1'
		JL BADINPUT1
		
		JMP NOTBAD
		
	BADINPUT1:
		; verifica daca nu e escape
		CMP AL, 27
		JNE BAD1	; daca e escape
		CALL clear_screen
		MOV AH, 4CH ; iesire in DOS
		INT 21H
	
	BAD1:
		CALL clear_screen
	
		MOV AH, 09h
		LEA DX, badinputstring
		INT 21h
		
		CALL read_ch ; asteapta tasta
		
		CALL clear_screen
		
		JMP MAINMENU
		
	NOTBAD:
		
		MOV BH, 0	; in functie de codul introdus de la tastatura, apeleaza o anumita procedura
		MOV BL, AL	; cu ajutorul tabelului declarat in zona de memorie
		SUB BL, '0'
		SUB BL, 1
		ADD BL, BL
		CALL WORD PTR tab_proc[BX]

		
		CALL read_ch	; asteapta un caracter pentru ieseirea din modul grafic
		
		MOV AX, 3 ; revenire mod alfanumeric (BIOS)
		INT 10H
		JMP MAINMENU
		
		CALL clear_screen
		
				
		
		
	read_ch PROC NEAR ; citeste un caracter de la tastatura
		MOV AH, 01h	
		INT 21h
		
		RET
	read_ch	ENDP
		
	clear_screen PROC NEAR
		MOV AX, 10H	; clear screen
		INT 10H
		MOV AX, 3 
		INT 10H
		
		RET
	ENDP
	
	in_pixel PROC NEAR	; procedura care genereaza interfata pentru a afisa un pixel. Aici se citesc coordonatele si se apeleaza procedura de desenare
		CALL clear_screen
		
		JMP BEGINPIX0
		
	BEGINPIX:
	
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstring1
		INT 21h
		
	BEGINPIX0:
	
		MOV AH, 09h
		LEA DX, pixelstring0
		INT 21h
	
	; read x
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTPIX1
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINPIX
		
	NEXTTESTPIX1:
		CMP AX, 640		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKPIX1
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINPIX
	
	INPUTOKPIX1:
		MOV DS:pixel_x, AX 
		JMP BEGINPIXY0
		
	BEGINPIXY:
		MOV AH, 09h
		LEA DX, badinputstring2
		INT 21h
		
	BEGINPIXY0:
		MOV AH, 09h
		LEA DX, pixelstring1
		INT 21h
		
	; read y
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTPIX2
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINPIXY
		
	NEXTTESTPIX2:
		CMP AX, 350		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKPIX2
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINPIXY

	INPUTOKPIX2:
		MOV DS:pixel_y, AX
	
		CALL clear_screen
		JMP PIXELREADCOLOR0
	
	PIXELREADCOLOR:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringCOLOR
		INT 21h
	PIXELREADCOLOR0:
		CALL read_color
		CMP AL, -1		; if bad input on reading color
		JE PIXELREADCOLOR
		
		MOV AX, 10H ; mod grafic 640 x 350
		INT 10H
		
		afisare_pixel DS:pixel_x, DS:pixel_y, DS:ct_color
		
		RET
	in_pixel ENDP
	
	in_line PROC NEAR	; se citesc coordonatele si se apeleaza procedura de desenare linie
		CALL clear_screen
		
		JMP BEGINLINE0
	
	BEGINLINE:
		CALL clear_screen
		
		MOV AH, 09h
		LEA DX, badinputstring1
		INT 21h
	
	BEGINLINE0:
		MOV AH, 09h
		LEA DX, linestring0
		INT 21h
	
	; read x1
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTLINE1
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINE
		
	NEXTTESTLINE1:
		CMP AX, 640		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKLINE1
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINE
	
	INPUTOKLINE1:
		MOV DS:line_x1, AX 
		
		
		JMP BEGINLINEY10
		; read y1
	BEGINLINEY1:
		MOV AH, 09h
		LEA DX, badinputstring2
		INT 21h
	BEGINLINEY10:
		MOV AH, 09h
		LEA DX, linestring1
		INT 21h
	
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTLINE2
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEY1
		
	NEXTTESTLINE2:
		CMP AX, 350		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKLINE2
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEY1
	
	INPUTOKLINE2:
		MOV DS:line_y1, AX 
		
		
		JMP BEGINLINEX20
		; read x2
	BEGINLINEX2:
		MOV AH, 09h
		LEA DX, badinputstring1
		INT 21h
	BEGINLINEX20:
		MOV AH, 09h
		LEA DX, linestring2
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTLINE3
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEX2
		
	NEXTTESTLINE3:
		CMP AX, 640		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKLINE3
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEX2
	
	INPUTOKLINE3:
		MOV DS:line_x2, AX 
			
		
		JMP BEGINLINEY20
		; read y2
	BEGINLINEY2:
		MOV AH, 09h
		LEA DX, badinputstring2
		INT 21h
	BEGINLINEY20:	
		MOV AH, 09h
		LEA DX, linestring3
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTLINE4
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEY2
		
	NEXTTESTLINE4:
		CMP AX, 350		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKLINE4
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINLINEY2
	
	INPUTOKLINE4:
		MOV DS:line_y2, AX 
		
		CALL clear_screen
		JMP LINEREADCOLOR0
		
	LINEREADCOLOR:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringCOLOR
		INT 21h
	LINEREADCOLOR0:
		CALL read_color
		CMP AL, -1		; if bad input on reading color
		JE LINEREADCOLOR
		
		MOV AX, 10H ; mod grafic 640 x 350
		INT 10H
		
		afisare_linie DS:line_x1, DS:line_y1, DS:line_x2, DS:line_y2, DS:ct_color
	
		RET
	in_line ENDP

	in_poli PROC NEAR	; se citeste numarul de varfuri, coordonatele fiecaruia si se apeleaza procedura de desenare poligon
		CALL clear_screen
		
		JMP BEGINPOLI0
	
	BEGINPOLI:
		CALL clear_screen
	
		MOV AH, 09h
		LEA DX, badinputstringNR
		INT 21h
	
	BEGINPOLI0:
	
		MOV AH, 09h
		LEA DX, polistring0
		INT 21h
	
		
		; read nr lines
		
		CALL read_3ch
		
		CMP AL, -1 ; if a bad input was given
		JNE NEXTTESTPOLIN
		JMP BEGINPOLI
		
	NEXTTESTPOLIN:
		CMP AX, 36 ; max 36 vertices for one polygon
		JG BEGINPOLI
		
		MOV CX, AX
		MOV DS:poli_n, AX
		MOV SI, 0
		MOV BL, 1
		
		JMP LOOPVERTICES
		
	LOOPVERTICES0:
		MOV AH, 09h
		LEA DX, badinputstring1
		INT 21h
	LOOPVERTICES:
		MOV AH, 09h
		LEA DX, polistringCT_VERTEX
		INT 21h
		
		MOV AH, 02h
		MOV DL, BL
		ADD DL, '0'
		INT 21h
		
		; read ct x
		MOV AH, 09h
		LEA DX, polistringx
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTPOLIX
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP LOOPVERTICES0
		
	NEXTTESTPOLIX:
		CMP AX, 640		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKPOLIX
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP LOOPVERTICES0
		
	INPUTOKPOLIX:
		MOV DS:poli[SI], AX
		INC SI
		INC SI
		
		JMP LOOPREADY0
	
	LOOPREADY:
		MOV AH, 09h
		LEA DX, badinputstring2
		INT 21h
	LOOPREADY0:
		; read ct y
		MOV AH, 09h
		LEA DX, polistringy
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTPOLIY
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP LOOPREADY
		
	NEXTTESTPOLIY:
		CMP AX, 350		; verifica daca coordonata se afla in limitele admise
		JLE INPUTOKPOLIY
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP LOOPREADY
		
	INPUTOKPOLIY:
		MOV DS:poli[SI], AX
		INC SI
		INC SI
		
		INC BL
		LOOP LOOPVERTICES
		
		
		; close the polygon by adding the coordinates of the first vertex to the array at the end
		MOV AX, DS:poli[0]
		MOV DS:poli[SI], AX
		INC SI
		INC SI
		
		MOV AX, DS:poli[2]
		MOV DS:poli[SI], AX
		INC SI
		INC SI
		
		CALL clear_screen
		JMP POLIREADCOLOR0
		
	POLIREADCOLOR:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringCOLOR
		INT 21h
	POLIREADCOLOR0:
		CALL read_color
		CMP AL, -1		; if bad input on reading color
		JE POLIREADCOLOR
		
		CALL clear_screen
		JMP POLIREADFILL0
		
	POLIREADFILL:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringFILL
		INT 21h
	POLIREADFILL0:
		CALL read_fill
		CMP AL, -1		; if bad input on reading fill
		JE POLIREADFILL
		
		
		MOV AX, 10H ; mod grafic 640 x 350
		INT 10H
		
		afisare_poligon DS:poli_n, DS:poli, DS:ct_color, DS:ct_fill
	
		RET
	in_poli ENDP
	
	in_cerc PROC NEAR ; se citesc coordonatele centrului si raza si se apeleaza procedura de desenare cerc
		CALL clear_screen
		JMP BEGINCERC0
	
	BEGINCERC:
		CALL clear_screen
		
		MOV AH, 09h
		LEA DX, badinputstring1
		INT 21h
		
	BEGINCERC0:
	
		MOV AH, 09h
		LEA DX, cercstring0
		INT 21h
	
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTCERCX
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINCERC
		
	NEXTTESTCERCX:
		CMP AX, 640		; verifica daca coordonata se afla in limitele admise
		JL INPUTOKCERCX
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINCERC
	
	INPUTOKCERCX:
		MOV DS:cerc_xc, AX
	
		
		JMP BEGINCERCY0
		; read ycent
	BEGINCERCY:
		MOV AH,09h
		LEA DX, badinputstring2
		INT 21h
		
	BEGINCERCY0:
		
		MOV AH, 09h
		LEA DX, cercstring1
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTCERCY
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINCERCY
		
	NEXTTESTCERCY:
		CMP AX, 350		; verifica daca coordonata se afla in limitele admise
		JL INPUTOKCERCY
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINCERCY
		
	INPUTOKCERCY:
		MOV DS:cerc_yc, AX
		
		JMP BEGINCERCR0
		; read radius
	BEGINCERCR:
		MOV AH,09h
		LEA DX, badinputstringRADIUS
		INT 21h
		
		MOV AX, CX
		CALL write_3ch
		
	BEGINCERCR0:
		MOV AH, 09h
		LEA DX, cercstring2
		INT 21h
		
		CALL read_3ch
		
		CMP AL, -1		; verifica daca nu a fost introdus ceva care nu corespunde cu asteptarile programului
		JNE NEXTTESTCERCR
		; daca nu este ok din orice motiv, se reseteaza citirea
		JMP BEGINCERCR
		
	NEXTTESTCERCR:
		MOV BX, AX
		
		MOV CX, 1000
	
		; compute the minimum value accepted for the radius such that the circile remains in the screen
	
		MOV DX, 640			; distance from center to right border
		MOV AX, DS:cerc_xc	
		SUB DX, AX
		CMP DX, CX
		JG FINDMIN1
		MOV CX, DX
			
	FINDMIN1:
		MOV DX, DS:cerc_xc	; distance from center to left border
		CMP DX, CX
		JG FINDMIN2
		MOV CX, DX

	FINDMIN2:
		MOV DX, 350
		MOV AX, DS:cerc_yc	; distance from center to bottom border
		SUB DX, AX
		CMP DX, CX
		JG FINDMIN3
		MOV CX, DX
			
	FINDMIN3:
		MOV DX, DS:cerc_yc	; distance from center to top border
		CMP DX, CX
		JG FINDMIN4
		MOV CX, DX
		
	FINDMIN4:
		MOV AX, BX
		CMP AX, CX
		JLE INPUTOKCERCR
		JMP BEGINCERCR
		
	INPUTOKCERCR:
		MOV DS:cerc_r, AX
		
		CALL clear_screen
		JMP CERCREADCOLOR0
		
	CERCREADCOLOR:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringCOLOR
		INT 21h
	CERCREADCOLOR0:
		CALL read_color
		CMP AL, -1		; if bad input on reading color
		JE CERCREADCOLOR
		
		CALL clear_screen
		JMP CERCREADFILL0
		
	CERCREADFILL:
		CALL clear_screen
		MOV AH, 09h
		LEA DX, badinputstringFILL
		INT 21h
	CERCREADFILL0:
		CALL read_fill
		CMP AL, -1		; if bad input on reading fill
		JE CERCREADFILL
		
		
		MOV AX, 10H ; mod grafic 640 x 350
		INT 10H
	
		afisare_cerc DS:cerc_xc, DS:cerc_yc, DS:cerc_r, DS:ct_color, DS:ct_fill
	
		RET
	in_cerc ENDP
	
	read_3ch PROC NEAR ; procedura care citeste un numar cu 3 cifre si il pune in registrul AX
		
		PUSH CX
		
		MOV DX, 0
		MOV CX, 3
		
	LOOP3CH:
		MOV AX, DX	; DX = DX * 10
		MOV DX, 10
		MUL DX
		MOV DX, AX
		
		CALL read_ch
		
		CMP AL, '0'	; verifica daca e cifra
		JL BADCH
		CMP AL, '9'
		JG BADCH
		
		MOV AH, 0
		SUB AX, '0'	; transforma in numar
		ADD DX, AX
		LOOP LOOP3CH
		JMP ENDREAD3CH
		
	BADCH:
		CMP AL, 13	; verifica daca nu e enter
		JE DIVIDE	; daca e enter, atunci numarul e gata (e mai mic decat 100)
		
		MOV AL, -1	; daca nu e enter e un input gresit
		POP CX
		RET
	
	DIVIDE:	; mai trebuie impartit o data la 10 deoarece inmultirea cu 10 s-a facut inainte de citirea caracterului
		CMP DX, 0
		JNE INPUTNOTZERO
		
		; if enter was pressed before writing any number
		MOV AL, -1
		POP CX
		RET
		
	INPUTNOTZERO:
		MOV AX, DX	; DX = DX / 10
		MOV DL, 10
		DIV DL
		MOV DX, AX
	
	ENDREAD3CH:
		MOV AX, DX	; salveaza numarul citit in registrul AX
		
		POP CX
		RET
	read_3ch ENDP
	
	read_color PROC NEAR ; creeaza interfata pentru citirea culorii. Culoarea este scrisa in memorie
		
		MOV AH, 09h
		LEA DX, colorstring
		INT 21h
		
		CALL read_ch
		
		CMP AL, '0'		; if AL >= 0
		JL NOT09COLOR
		
		CMP AL, '9'		; if AL <= 9
		JG NOT09COLOR
		JMP COLOROK1
		
	NOT09COLOR:
		CMP AL, 'A'		; if AL >= A
		JL BADCOLOR0
		
		CMP AL, 'F'		; if AL <= F
		JG BADCOLOR0
		JMP COLOROK2
		
	BADCOLOR0:		; verificam sa nu fie lower case
		CMP AL, 'a'
		JL BADCOLOR
		
		CMP AL, 'f'
		JG BADCOLOR
		JMP COLOROK3
		
	BADCOLOR:
		MOV AL, -1
		RET
	
	COLOROK1:
		MOV AH, 0
		SUB AL, '0'
		MOV DS:ct_color, AX
		RET
		
	COLOROK2:
		MOV AH, 0
		SUB AL, 'A'
		ADD AL, 10
		MOV DS:ct_color, AX	
		RET
		
	COLOROK3:
		MOV AH, 0
		SUB AL, 'a'
		ADD AL, 10
		MOV DS:ct_color, AX	
		RET
	read_color ENDP
	
	read_fill PROC NEAR ; creeaza interfata pentru citirea modului de fill. modul de fill este salvat in memorie
		MOV AH, 09h
		LEA DX, fillstring
		INT 21h
		
		CALL read_ch
		
		CMP AL, '0'
		JE FILLOK
		
		CMP AL, '1'
		JE FILLOK
		
		MOV AL, -1	; if anything else except 0 or 1 was given as input return -1
		RET
	
	FILLOK:
		SUB AL, '0'
		MOV AH, 0
		MOV DS:ct_fill, AX
		RET
	read_fill ENDP
	
	write_3ch PROC NEAR ; afiseaza un numar de 3 cifre memorat in AX.
		
		PUSH AX
		
		CMP AX, 100
		JL NOHUNDREDS
		
		MOV AH, 0
		MOV BL, 100	; print the hundreds
		DIV BL
		
		PUSH AX
		
		ADD AL, '0'
		MOV AH, 02h
		MOV DL, AL
		INT 21h
		
		JMP GREATERHUNDRED
		
	NOHUNDREDS:
		
		CMP AX, 10
		JL NOTENS
		
		JMP GREATERTENS
		
	GREATERHUNDRED:
		POP AX
		MOV AL, AH
		MOV AH, 0
		
	GREATERTENS:
		MOV AH, 0
		MOV BL, 10	; print the tens
		DIV BL
		
		PUSH AX
		
		ADD AL, '0'
		MOV AH, 02h
		MOV DL, AL
		INT 21h
		
		
	
		POP AX		
		ADD AH, '0'
		MOV DL, AH
		JMP PRINTONES
		
	NOTENS:	
		ADD AL, '0'
		MOV DL, AL
		
	PRINTONES:
		MOV AH, 02h
		INT 21h
		
		POP AX
		
		RET
	write_3ch ENDP
	
CODE ENDS

END START