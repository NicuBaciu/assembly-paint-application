sablon1 STRUC
	BP1 DW ?
	AE1 DW ?
	AS1 DW ?
	px DW ?
	py DW ?
	pculoare DW ?
sablon1 ENDS

sablon2 STRUC
	lnumerator DW ?
	llongest DW ?
	lshortest DW ?
	lNdeltax DW ?
	lNdeltay DW ?
	dx1 DW ?
	dx2 DW ?
	dy1 DW ?
	dy2 DW ?
	lp2 DW ?
	lp1 DW ?
	ldeltay DW ?
	ldeltax DW ?
	BP2 DW ?
	AE2 DW ?
	AS2 DW ?
	lx1 DW ?
	ly1 DW ?
	lx2 DW ?
	ly2 DW ?
	lculoare DW ?
sablon2 ENDS

sablon3 STRUC
	BP3 DW ?
	AE3 DW ?
	AS3 DW ?
	tx1 DW ?
	ty1 DW ?
	tx2 DW ?
	ty2 DW ?
	tx3 DW ?
	ty3 DW ?
	tculoare DW ?
	tfill DW ?
sablon3 ENDS

sablon4 STRUC
	BP3 DW ?
	AE4 DW ?
	AS4 DW ?
	poln DW ?
	polDS DW ?
	polvector DW ?
	polculoare DW ?
	polfill DW ?
sablon4 ENDS

sablon5 STRUC
	cerx DW ?
	cery DW ?
	cerp DW ?
	BP4 DW ?
	AE5 DW ?
	AS5 DW ?
	cercxc DW ?
	cercyc DW ?
	cercr DW ?
	cercculoare DW ?
	cercfill DW ?
sablon5 ENDS

DATA3 SEGMENT PARA PUBLIC 'DATA'
	ymin DW ?
	ymax DW ?
	
	m DD 40 dup(?)
	n DD 40 dup(?)
	
	xbeg DW 40 dup(?)
	xend DW 40 dup(?)
	
	ybeg DW 40 dup(?)
	yend DW 40 dup(?)
	
	x DW 40 dup(?)
	
	x1 DW ?
	x2 DW ?
	
	ct_line DW 0
	ct_posW DW 0
	
	flag_save_coord DW 1
	
	ct_color DW ?
	
	temp_word DW ?
DATA3 ENDS

INCLUDE C:\Tasm\a_pixel.mac
INCLUDE C:\Tasm\a_math.mac

PUBLIC print_pixel
PUBLIC print_line
PUBLIC print_tr
PUBLIC print_poligon
PUBLIC print_circle

EXTRN XCER:WORD
EXTRN YCER:WORD

EXTRN compute_coord:FAR

CODE1 SEGMENT PARA PUBLIC 'CODE'
	ASSUME CS:CODE1, DS:DATA3
	
	print_pixel PROC FAR
		
		PUSH BP
		MOV BP, SP
		
		PUSH AX 	; salvare reg
		PUSH CX
		PUSH DX
		
		MOV AX, [BP].pculoare
		MOV CX, [BP].px
		MOV DX, [BP].py		
		MOV AH, 12 	; set pixel
		INT 10H 	; desenare pixel (BIOS)
		
		POP DX 		;restaurare registre
		POP CX
		POP AX
		POP BP
		
		RET 6
	print_pixel ENDP
	
	print_line PROC FAR		
		PUSH BP
		SUB SP, 26		; alocare spatiu variabile locale
		MOV BP, SP
		
		PUSH AX			; salvare registre
		PUSH CX
		PUSH SI
		
	; ====== Ndeltax, Ndeltay ======
		
		MOV AX, [BP].lx2 ; Ndeltax = x2 - x1
		SUB AX, [BP].lx1
		MOV [BP].lNdeltax, AX
		
		MOV AX, [BP].ly2 ; Ndeltay = y2 - y1
		SUB AX, [BP].ly1
		MOV [BP].lNdeltay, AX
		
	; ======== abs(deltax) ==========
		MOV AX, [BP].lx1
		CMP [BP].lx2, AX
		JGE LXDELTA2
	; if x2 < x1
		MOV AX, [BP].lx1
		SUB AX, [BP].lx2
		JMP LXDELTAEND
	; else
	LXDELTA2:
		MOV AX, [BP].lx2
		SUB AX, [BP].lx1
	LXDELTAEND:
	;end if
	
		MOV [BP].ldeltax, AX
		
	; ========= abs(deltay) ==========
		MOV AX, [BP].ly1
		CMP [BP].ly2, AX
		JGE LYDELTA2
	; if y2 < y1
		MOV AX, [BP].ly1
		SUB AX, [BP].ly2
		JMP LYDELTAEND
	; else
	LYDELTA2:
		MOV AX, [BP].ly2
		SUB AX, [BP].ly1
	LYDELTAEND:
	;end if
	
		MOV [BP].ldeltay, AX
		
		PUSH DS
		
		MOV AX, DATA3			; set the data segment to DATA3
		MOV DS, AX
		
		MOV AX, DS:flag_save_coord ; if we have to store the coordinates of the line
		CMP AX, 1				; i.e. the line is a border of the shape, not a filling line
		JL NOTSTORECOORD0
		JMP NOTSTORECOORD1
	NOTSTORECOORD0:
		JMP NOTSTORECOORD
	NOTSTORECOORD1:
		
		MOV SI, DS:ct_posW
		
	; ======== compute xbeg and xend ==========
	
		MOV AX, [BP].lx1
		CMP [BP].lx2, AX
		JG X2X1
	; if x1 > x2
		
		MOV AX, [BP].lx2
		MOV DS:xbeg[SI], AX
		MOV AX, [BP].lx1
		MOV DS:xend[SI], AX
		JMP X2X1END
		
	; else
	X2X1:
		
		MOV AX, [BP].lx1
		MOV DS:xbeg[SI], AX
		MOV AX, [BP].lx2
		MOV DS:xend[SI], AX
		
	; end if
	X2X1END:
	
	; ======== compute ybeg and yend ===========
	
		MOV AX, [BP].ly1
		CMP [BP].ly2, AX
		JG Y2Y1
	; if y1 > y2
		
		MOV AX, [BP].ly2
		MOV DS:ybeg[SI], AX
		MOV AX, [BP].ly1
		MOV DS:yend[SI], AX
		JMP Y2Y1END
		
	; else
	Y2Y1:
		
		MOV AX, [BP].ly1
		MOV DS:ybeg[SI], AX
		MOV AX, [BP].ly2
		MOV DS:yend[SI], AX
		
	; end if
	Y2Y1END:
	
		ADD SI, 2
		MOV DS:ct_posW, SI
		
		MOV SI, DS:ct_line
		
	; ======== compute slope =========
	
		FINIT
		
		FILD [BP].ly2
		FILD [BP].ly1
		FSUB
		FILD [BP].lx2
		FILD [BP].lx1
		FSUB
		FDIV
		FST DS:m[SI]
		
		FWAIT
	
	; ===== compute y-intercept ======
	
		FINIT
		
		FILD [BP].ly1		; n = y1 - m * x1
		FLD DS:m[SI]
		FILD [BP].lx1
		FMUL
		FSUB
		FST DS:n[SI]
		
		FWAIT
	
		MOV AX, DS:ct_line	; increment nr of lines
		ADD AX, 4
		MOV DS:ct_line, AX
		
	; ===== compute ymin and ymax ======
	
		; compute YMAX
		MOV AX, DS:ymax
		CMP [BP].ly1, AX
		JL COMPY2MAX
	; if y1 > ymax	
		MOV AX, [BP].ly1
		MOV DS:ymax, AX
		
	COMPY2MAX:
		MOV AX, DS:ymax
		CMP [BP].ly2, AX
		JL NOYMAX
	; if y2 > ymax
		MOV AX, [BP].ly2
		MOV DS:ymax, AX
		
	NOYMAX:
		
		; compute YMIN
		MOV AX, DS:ymin
		CMP [BP].ly1, AX
		JG COMPY2MIN
	; if y1 < ymin
		MOV AX, [BP].ly1
		MOV DS:ymin, AX
	
	COMPY2MIN:
		MOV AX, DS:ymin
		CMP [BP].ly2, AX
		JG NOYMIN
	; if y2 < ymin
		MOV AX, [BP].ly2
		MOV DS:ymin, AX
		
	NOYMIN:
	
	NOTSTORECOORD:
		POP DS
	
	; ======== init dx, dy ===========
		MOV AX, 0
		MOV [BP].dx1, AX
		MOV [BP].dy1, AX
		MOV [BP].dx2, AX
		MOV [BP].dy2, AX
		
		MOV AX, [BP].lNdeltax
		CMP AX, 0
		JL LDX1
		JG LDX2
	; if (Ndeltax == 0) dx1, dx2 = 0
		JMP LDXEND
	; else if (Ndeltax > 0) dx1, dx2 = 1
	LDX2:
		MOV AX, 1
		MOV [BP].dx1, AX
		MOV [BP].dx2, AX
		JMP LDXEND
	; else if (Ndeltax < 0) dx1, dx2 = -1
	LDX1:
		MOV AX, -1
		MOV [BP].dx1, AX
		MOV [BP].dx2, AX
	LDXEND:
	; end if
	
	
		MOV AX, [BP].lNdeltay
		CMP AX, 0
		JL LDY1
		JG LDY2
	; if (Ndeltay == 0) dy1 = 0
		JMP LDYEND
	; else if (Ndeltay > 0) dy1 = 1
	LDY2:
		MOV AX, 1
		MOV [BP].dy1, AX
		JMP LDYEND
	; else if (deltay < 0) dy1 = -1
	LDY1:
		MOV AX, -1
		MOV [BP].dy1, AX
	LDYEND:
	; end if

	; ======== determine longest and shortest =========
	
		MOV AX, [BP].ldeltax
		CMP [BP].ldeltay, AX
		JGE LLONGY
	; if (deltax > deltay)
		MOV AX, [BP].ldeltax
		MOV [BP].llongest, AX
		MOV AX, [BP].ldeltay
		MOV [BP].lshortest, AX
		JMP LLONGEND
	; else if (deltay > deltax)
	LLONGY:
		MOV AX, [BP].ldeltay
		MOV [BP].llongest, AX
		MOV AX, [BP].ldeltax
		MOV [BP].lshortest, AX
		
		; modify dy2 and dx2
		
		MOV AX, 0 ; dx2 = 0
		MOV [BP].dx2, AX
		
		MOV AX, [BP].lNdeltay
		CMP AX, 0
		JL LDYY1
		JG LDYY2
	; if (Ndeltay == 0)
		JMP LDYY0
	; else if (Ndeltay < 0)
	LDYY1:
		MOV AX, -1
		MOV [BP].dy2, AX
		JMP LDYY0
	; else if (Ndeltay > 0)
	LDYY2:
		MOV AX, 1
		MOV [BP].dy2, AX
	LDYY0:
	; end if
	
	LLONGEND:
	; end if
		
	; ========= init numerator ==========
		MOV AX, [BP].llongest
		SHR AX, 1
		MOV [BP].lnumerator, AX
		
	; ========= main loop for drawing ==========
		MOV CX, [BP].llongest
		
	LMAINLOOP:
		afisare_pixel [BP].lx1, [BP].ly1, [BP].lculoare
		
		MOV AX, [BP].lshortest ; numerator += shortest
		ADD [BP].lnumerator, AX
		
		MOV AX, [BP].lnumerator
		CMP AX, [BP].llongest
		JGE LNEXTXY1
	; if (numerator < longest)
		MOV AX, [BP].dx2 ; x += dx2
		ADD [BP].lx1, AX
		MOV AX, [BP].dy2 ; y += dy2
		ADD [BP].ly1, AX
		JMP LNEXTXY0
	; else if (numerator > longest)
	LNEXTXY1:
		MOV AX, [BP].dx1 ; x += dx1
		ADD [BP].lx1, AX
		MOV AX, [BP].dy1 ; y += dy1
		ADD [BP].ly1, AX
		MOV AX, [BP].llongest ; numerator -= longest
		SUB [BP].lnumerator, AX
	LNEXTXY0:
	; end if
		LOOP LMAINLOOP
	
		POP SI
		POP CX			; restaurare registre
		POP AX			
		ADD SP, 26 		; stergerea variabilelor locale
		POP BP
		
		RET 10
	print_line ENDP
	
	print_tr PROC FAR
		PUSH BP
		MOV BP, SP
		
		PUSH AX
		
		PUSH DS	; save current data segment
		
		MOV AX, DATA3			; set the data segment to DATA3
		MOV DS, AX
		
		MOV AX, [BP].tculoare	; setting the current color for eventual filling
		MOV DS:ct_color, AX
		
		MOV AX, 1		; setting the flag on one to let the print line procedure know that we want the coordinates of the line to be saved
		MOV DS:flag_save_coord, AX
		
		MOV AX, 1000
		MOV DS:ymin, AX ; initializare ymin pentru a putea calcula minimul
		
		MOV AX, 0
		MOV DS:ymax, AX ; initializare ymax pentru a putea calcula maximul
		
		MOV AX, 0
		MOV DS:ct_posW, AX ; intializare indexul de salvare a parametriilor ecuatiei dreptei pentru umplerea unei noi forme
		
		MOV AX, 0
		MOV DS:ct_line, AX ; initializarea numarului de linii ale unei forme pentru a putea umple o forma noua
		
		POP DS ; restore data segment
		
		afisare_linie [BP].tx1, [BP].ty1, [BP].tx2, [BP].ty2, [BP].tculoare	
		afisare_linie [BP].tx3, [BP].ty3, [BP].tx2, [BP].ty2, [BP].tculoare
		afisare_linie [BP].tx1, [BP].ty1, [BP].tx3, [BP].ty3, [BP].tculoare
		
		CMP [BP].tfill, 0
		JE NOFILLTR
		CALL fill
	
	NOFILLTR:
	
		POP AX
		POP BP
		
		RET 16
	print_tr ENDP
	
	print_poligon PROC FAR
		PUSH BP
		MOV BP, SP
		
		PUSH AX		; salvare registre
		PUSH CX
		PUSH BX
		PUSH SI
		PUSH DS
		
		PUSH DS ; save current data segment
		
		MOV AX, DATA3			; set the data segment to DATA3
		MOV DS, AX
		
		MOV AX, [BP].polculoare	; setting the current color for eventual filling
		MOV DS:ct_color, AX
		
		MOV AX, 1
		MOV DS:flag_save_coord, AX
		
		MOV AX, 1000
		MOV DS:ymin, AX ; initializare ymin pentru a putea calcula minimul
		
		MOV AX, 0
		MOV DS:ymax, AX ; initializare ymax pentru a putea calcula maximul
		
		MOV AX, 0
		MOV DS:ct_posW, AX ; intializare indexul de salvare a parametriilor ecuatiei dreptei pentru umplerea unei noi forme
		
		MOV AX, 0
		MOV DS:ct_line, AX ; initializarea numarului de linii ale unei forme pentru a putea umple o forma noua
		
		POP DS 	; restore data segment
		
		MOV BX, [BP].polvector
		MOV CX, [BP].poln
		MOV AX, [BP].polDS
		MOV DS, AX
		MOV SI, 0
		
	POLMAINLOOP:
		afisare_linie [BX+SI], [BX+SI+2], [BX+SI+4], [BX+SI+6], [BP].polculoare
		ADD SI, 4
		LOOP POLMAINLOOP
		
		CMP [BP].polfill, 0
		JE NOFILLPOL
		CALL fill
	
	NOFILLPOL:
		
		POP DS		; restaurare registre
		POP SI		
		POP BX
		POP CX
		POP AX
		
		POP BP
		
		RET 10
	print_poligon ENDP
	
	print_circle PROC FAR
		PUSH BP
		SUB SP, 6 	; alocare memorie pentru parametri locali
		MOV BP, SP ; initializare BP pentru transmiterea parametrilor prin stiva
		
		PUSH AX	; salvare registre
		PUSH BX
		PUSH CX
		PUSH SI
		
		PUSH DS	; save current data segment
		
		MOV AX, DATA3			; set the data segment to DATA3
		MOV DS, AX
		
		MOV AX, [BP].tculoare	; setting the current color for eventual filling
		MOV DS:ct_color, AX
		
		MOV AX, 1
		MOV DS:flag_save_coord, AX
		
		compute_coordinates [BP].cercxc, [BP].cercyc, [BP].cercr ; calculeaza coordonatele punctelor de pe cerc
		
		
		PUSH DS 
		
		MOV AX, DATA3
		MOV DS, AX
		
		MOV AX, 1000
		MOV DS:ymin, AX ; initializare ymin pentru a putea calcula minimul
		
		MOV AX, 0
		MOV DS:ymax, AX ; initializare ymax pentru a putea calcula maximul
		
		MOV AX, 0
		MOV DS:ct_posW, AX ; intializare indexul de salvare a parametriilor ecuatiei dreptei pentru umplerea unei noi forme
			
		MOV AX, 0
		MOV DS:ct_line, AX ; initializarea numarului de linii ale unei forme pentru a putea umple o forma noua
			
		POP DS
		
		MOV AX, 1
		MOV CX, 35	; se calculeaza 36 de puncte pentru un cerc si se traseaza linii intre acele puncte
		MOV SI, 0
		
	PRINTCERCCOORD:
		afisare_linie XCER[SI], YCER[SI], XCER[SI+2], YCER[SI+2], [BP].cercculoare ; trasare linii
		INC SI	; trecere la urmatoarea pereche de coordonate
		INC SI
		
		SUB CX, AX
		CMP CX, 0
		JG PRINTCERCCOORD
		
		afisare_linie XCER[SI], YCER[SI], XCER[0], YCER[0], [BP].cercculoare ; la sfarsit se traseaza o linie intre ultimul punct calculat si primul pentru a inchide cercul
		POP DS	; restore data segment
		
		CMP [BP].cercfill, 0
		JE NOFILLCER
		CALL fill
	
	NOFILLCER:
		
		POP SI
		POP CX
		POP BX
		POP AX ; restaurare registre
		
		ADD SP, 6 ; eliberare memorie alocata pentru parametri locali
		POP BP
		
		RET 10
	print_circle ENDP
	
	fill PROC NEAR
			
		PUSH AX
		PUSH CX
		PUSH BX
		PUSH SI
		PUSH DX
		PUSH BP
		
		PUSH DS	; save current data segment
		
		MOV AX, DATA3			; set the data segment to DATA3
		MOV DS, AX
		
		MOV AX, 0
		MOV DS:flag_save_coord, AX
		
		; we have to go through each horizontal line from y = ymin to y = ymax
		; and calculate the points of intersection with the lines forming the shape
		
		MOV CX, DS:ymin
		
	LOOP1:
		
		MOV BP, 0 ; ct intersection
		MOV BX, 0 ; index for n and m
		MOV SI, 0 ; index for xbeg and xend
		
	LOOP2:
		
		FINIT
		FLD DS:m[BX]
		
		MOV AX, 10
		MOV DS:temp_word, AX

		FILD DS:temp_word
		FMUL		
		FIST DS:temp_word
		FWAIT
		
		MOV AX, DS:temp_word
		CMP AX, 0
		JE HORIZLINE
		
		; for the vertical lines the intersection cannot be calculated based on the equation
		; a direct aproach is needed
		
		MOV AX, DS:xbeg[SI]
		CMP AX, DS:xend[SI]
		JE VERTICLINE
		
		; the intersection is verified by computing the value of x based on y, m and n
		; and checking if it is between the limits of the line we want to intersect
		FINIT
		
		MOV DS:temp_word, CX
		
		FILD temp_word
		FLD DS:n[BX]
		FSUB
		FLD DS:m[BX]
		FDIV
		FIST DS:temp_word			; AX = (y - n) / m
				
		FWAIT
		
		MOV AX, DS:xbeg[SI]
		
		CMP AX, DS:temp_word
	; if temp_word >= xbeg
		JG NOINTER
	; if temp_word <= xend
		MOV AX, DS:xend[SI]
		CMP AX, DS:temp_word
		JL NOINTER
		
		MOV AX, DS:temp_word
		
	CALCULATEENDPOINTS:
	; calculate all intersection points 
		MOV DS:x[BP], AX
		ADD BP, 2
		JMP NOINTER
		
	HORIZLINE:
		FINIT
		FLD DS:n[BX]
		FIST DS:temp_word
		FWAIT
		
		MOV AX, DS:temp_word	; verify if the horizontal line is the same as our yct
		CMP AX, CX			
		JNE NOINTER
		
		MOV AX, DS:xend[SI]		; we set the last point of the line as the "intersection point" of the line
		JMP CALCULATEENDPOINTS
		
	VERTICLINE:
		CMP CX, DS:ybeg[SI]
	; if y >= ybeg
		JL NOINTER
	; if y <= yend
		CMP CX, DS:yend[SI]
		JG NOINTER
	
		MOV AX, DS:xbeg[SI]
		JMP CALCULATEENDPOINTS
		
	NOINTER:
		
		ADD SI, 2
		ADD BX, 4
		CMP BX, DS:ct_line
		JL LOOP20
		JMP ENDLOOP2
	LOOP20:
		JMP LOOP2
		
	ENDLOOP2:
	
		SUB BP, 2
	
		PUSH BP
	
	; compute the minimal value for the line that we must draw to fill the shape
		MOV AX, DS:x[BP]
		MOV DS:x1, AX
	
	LOOPMINX:
		MOV AX, DS:x1
		CMP AX, DS:x[BP]
		JLE C2MIN
	; if x[BP] < xmin
		MOV AX, DS:x[BP]
		MOV DS:x1, AX
	C2MIN:
		SUB BP, 2
		CMP BP, 0
		JGE LOOPMINX
		
		POP BP
	
	; compute the maximal value for the line that we must draw to fill the shape
		MOV AX, DS:x[BP]
		MOV DS:x2, AX
		
	LOOPMAXX:
		MOV AX, DS:x2
		CMP AX, DS:x[BP]
		JGE C2MAX
	; if x[BP] > xmin
		MOV AX, DS:x[BP]
		MOV DS:x2, AX
	C2MAX:
		SUB BP, 2
		CMP BP, 0
		JGE LOOPMAXX
	
		afisare_linie DS:x1, CX, DS:x2, CX, DS:ct_color
	
		INC CX
		CMP CX, DS:ymax
		JL LOOP10
		JMP LOOP11
	LOOP10:
		JMP LOOP1
	LOOP11:
		
		POP DS
		
		POP BP
		POP DX
		POP SI
		POP BX
		POP CX
		POP AX
	
		RET
	fill ENDP
CODE1 ENDS
END